package pl.sda;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Podaj datę najblizszych zajęć w formie dd/MM/yyyy");
        Scanner sc = new Scanner(System.in);
        String nextDate = sc.next();

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate localDateObj = LocalDate.parse(nextDate, dateTimeFormatter);
        Period period = Period.between(LocalDate.now(), localDateObj);
        System.out.println(period);
    }
}
